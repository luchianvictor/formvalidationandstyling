
        /*

        Adding the modal

        */


        (function () {

        var modal = document.getElementById('addProduct');
        var btn = document.getElementById("openModal");
        var span = document.getElementsByClassName("close")[0];
        var cancel_form = document.getElementById("cancel");

        // When the user clicks on the button, open the modal 
        btn.onclick = function() {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // Close when user clicks on cancel button.
        cancel_form.onclick = function(e) {
            e.preventDefault();
            modal.style.display = "none";
        }

        // When the user clicks outside of the modal, close it
        window.onclick = function(e) {
            if (e.target == modal) {
                modal.style.display = "none";
            }
        }


        })();

        /*

         Adding the interactive fields

        */

            var damaged = document.querySelector('#damaged');
            var damageDet = document.querySelector('.damage-details')
            damaged.addEventListener('change', function (event) {
                if (damaged.checked) {
                    damageDet.style.display = "block";
                } else {
                    damageDet.style.display = "none";
                }
            });
            var brand_select = document.querySelector('.brand_select');
            var selected_brand = document.querySelector('.selected_brand');
            brand_select.addEventListener('change',function(event) {
            	if (brand_select.value !== '1') {
            		selected_brand.style.display = "block";
            		selected_brand.style.backgroundImage = "url(img/brands/"+brand_select.value + ".jpg)";
            	} else if (brand_select.value === '1') {
            		selected_brand.style.display = "none";
            	}
            })
            var color_select = document.querySelector('.color_select');
            var selected_color = document.querySelector('.selected_color');
            color_select.addEventListener('change',function(event) {
                if (color_select.value !== '1') {
                    selected_color.style.backgroundColor = color_select.value;
                } else if (color_select.value === '1') {
                    selected_color.style.backgroundColor = "white";
                }
            	
            });

        
        /*

        Checking the form

        */



        function checkform(form) {
            

            var errors = [];
            var announcement = form.announcement.value;
            var brand = form.brand.value;
            var year = form.year.value;
            var mileage = form.mileage.value;
            var fuelType = form.fuelType.value;
            var color = form.color.value;
            var damaged = form.damaged.checked;
            var damage = form.damage.value;
            var price = form.price.value;
            var currency = form.currency.value;
            var description = form.description.value;

            function displayError(arg,error) {
                errors.push(error);
                if (document.getElementById(arg).className.indexOf("red") === -1){
                document.getElementById(arg).className += " red";
                }
            }
            function displaySuc(arg) {
                document.getElementById(arg).className -= " red";
            }

            if (announcement === "") {
                displayError("announcement","announcement:Provide a title of announcement");
            } else {displaySuc("announcement")}
            if (brand === "1") {
                displayError("brand","brand:Select a brand from the list"); 
            } else {displaySuc("brand")}
            if (year === "ex: 2005") {
                displayError("year","year:Enter a year of manufacturing"); 
            } else if (isNaN(parseInt(year))) {
                displayError("year","year:Manufacture year should be a number");
            } else if (parseInt(year) < 1920 || parseInt(year) > new Date().getFullYear()) {
                displayError("year","year:Insert a proper year of manufacturing");
            } else {displaySuc("year")}
            if (mileage === "ex: 15000") {
                displayError("mileage","mileage:Provide a mileage number");  
            } else if (isNaN(parseInt(mileage))) {
                displayError("mileage","mileage:Mileage should be a number");
            } else {displaySuc("mileage")}

            if (fuelType === "1") {
                displayError("fuelType","fuelType:Select a fuel type");
            } else {displaySuc("fuelType")}
            if (color === "1") {
                displayError("color","color:Select a color");
            } else {displaySuc("color")}
            if (damaged === true) {
                if (damage === "" || damage.length < 10) {
                    displayError("damage","damage:Provide a proper description");  
                }
            } else {displaySuc("damage")}
            if (price === "ex: 15000") {
                displayError("price","price:Enter a price");
            } else if (isNaN(parseInt(price))) {
                displayError("price","price:Price should be a number");
            } else {displaySuc("price")}

            if (currency === "1") {
                displayError("currency","currency:Select a currency");
            } else {displaySuc("currency")}
            if (description === "") {
                displayError("description","description:Provide a description");
            } else if (description.length < 10) {
                displayError("description","description:The description should have more than ten characters")
            } else {displaySuc("description")}
            

            if (errors.length=== 0) {
                //submit form
                return true;
            } else {
                //display errors
                errBlock = document.querySelector(".errors");
                document.querySelector(".errors").style.display = "block";
                errBlock = document.querySelector(".error");
                errTitle = document.querySelector(".error-title");
                errBlock.innerHTML = "";
                errTitle.innerHTML = "";
                errTitle.innerHTML = "There has been an error with your submission. Please check the following fields: <br />";
                for(var i=0;i<errors.length;i++) {
                                var point = errors[i].indexOf(":");
                    var errLoc = errors[i].slice(0,point);
                    var errMsg = errors[i].slice(point+1,errors[i].length);
                    errBlock.innerHTML += "<a href='#"+errLoc+"'> - " + errMsg + "</a><br />";
                
                }

               return false; 
            }

            
        } // end checkform() 

